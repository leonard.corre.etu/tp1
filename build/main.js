"use strict";

var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];

function sortalpha(a, b) {
  if (a.name > b.name) {
    return 1;
  }

  if (b.name > a.name) {
    return -1;
  }

  return 0;
}

function sortpetitprixcroissant(a, b) {
  if (a.price_small > b.price_small) {
    return 1;
  }

  if (b.price_small > a.price_small) {
    return -1;
  }

  return sortgrandprixcroissant(a, b);
}

function sortgrandprixcroissant(a, b) {
  if (a.price_large > b.price_large) {
    return 1;
  }

  if (b.price_large > a.price_large) {
    return -1;
  }

  return 0;
}

data.sort(sortpetitprixcroissant);

function filtretomate(a) {
  return a.base == 'tomate';
}

function filtreprix(a) {
  return a.price_small < 6;
}

function filtre2i(a) {
  return a.name.split('i').length == 2;
}

var datafiltre = data.filter(filtre2i);
console.log(data);
var res = "";

for (var index = 0; index < datafiltre.length; index++) {
  var element = datafiltre[index];
  var urltmp = "".concat(element.image);
  var htmltmp = "<article class=\"pizzaThumbnail\"> <a href=\"".concat(urltmp, "\" > <img src=\"").concat(urltmp, "\" />\n     <section><h4>").concat(element.name, "</h4>\n        <ul><li> Prix petit format : \n        ").concat(element.price_small.toFixed(2), " \u20AC\n        </li><li>\n        Prix grand format : ").concat(element.price_large.toFixed(2), " \u20AC\n        </li>\n        </ul>\n     </section> </a></article>");
  res += htmltmp;
}

var data2 = data.map(function (_ref) {
  var name = _ref.name,
      price_large = _ref.price_large,
      price_small = _ref.price_small,
      image = _ref.image;
  var urltmp = image;
  var htmltmp = "<article class=\"pizzaThumbnail\"> <a href=\"".concat(urltmp, "\" > <img src=\"").concat(urltmp, "\" />\n     <section><h4>").concat(name, "</h4>\n        <ul><li> Prix petit format :\n        ").concat(price_small.toFixed(2), " \u20AC\n        </li><li>\n        Prix grand format : ").concat(price_large.toFixed(2), " \u20AC\n        </li>\n        </ul>\n     </section> </a></article>");
  return htmltmp;
}); // let res2 = ``;
// data.forEach(element => {
//     let urltmp = `images/${element.toLowerCase()}.jpg`;
//     let htmltmp = `<article class="pizzaThumbnail"> <a href="${urltmp}" > <img src="${urltmp}" /> <section>${element}</section> </a></article>`;
//     res2 += htmltmp;
// });

console.log(data2);
document.querySelector('.pageContent').innerHTML = data2.join();